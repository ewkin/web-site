import React, {useCallback, useEffect, useRef, useState} from 'react';
import axios from "axios";
import './FullPost.css';

const parseSearch = search => {
    const params = new URLSearchParams(search);
    return Object.fromEntries(params);
};

const FullPost = props => {
    const parsed = parseSearch(props.location.search);
    const id = useRef(parseInt(parsed.id));


    const [post, setPost] = useState(null);

    const  fetchData = useCallback(async () =>{
        if(id.current){
            const postResponse = await axios.get('posts/'+id.current);
            setPost(postResponse.data);
        }
    }, [id]);

    useEffect(()=>{
        fetchData().catch(console.error);
    }, [id, fetchData]);


    return post && (
        <article className="FullPost">
            <h1>{post.title}</h1>
            <p>{post.body}</p>
        </article>
    );
};

export default FullPost;