import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import './Main.css';


const Main = props => {
    return (
        <Grid item xs={12} md={8}>
            <Typography variant="h6" gutterBottom>
                {props.title}
            </Typography>
            <Divider />
            {props.posts.map((post) => (
                <article key={post.id} className="Post" onClick={()=>props.clicked(post.id)}>
                    <h1>{post.title}</h1>
                    <div className="Info">
                        <div className="Author">{post.author}</div>
                    </div>
                </article>
            ))}
        </Grid>
    );
};

export default Main;
