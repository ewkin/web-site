import React, {memo} from 'react';

const Messages = memo((props) => {
    if(props.author==='Ignat'){
        return (
            <div className="media media-chat media-chat-reverse ">
                <img className="avatar" src="https://img.icons8.com/color/36/000000/administrator-male.png" alt="..."/>
                <div className ="media-body">
                    <p className="meta" >{props.author}</p>
                    <p className = "text">{props.message}</p>
                    <p className ="meta"><time>{props.date}</time></p>
                </div>
            </div>
    )
    } else {
        return (
            <div className="media media-chat">
                <img className ="avatar" src="https://img.icons8.com/color/36/000000/administrator-male.png" alt="..."/>
                <div className ="media-body">
                    <p className ="meta">{props.author}</p>
                    <p>{props.message}</p>
                    <p className ="meta"><time>{props.date}</time></p>
                </div>
            </div>
        )
    }

});

export default Messages;