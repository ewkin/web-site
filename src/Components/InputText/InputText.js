import React from 'react';

const InputText = props => {
    return (
        <div className="publisher bt-1 border-light"><img className="avatar avatar-xs" src="https://img.icons8.com/color/36/000000/administrator-male.png" alt="..."/>
            <form onSubmit={props.handleSubmit}>
                <input className="publisher-input" type="text" value={props.value} onChange={props.handleChange} placeholder="Write something" required/>
                <button type="submit" className="publisher-btn text-info" id="send" href="#" data-abc="true">
                    <i className="fa fa-paper-plane"></i>
                </button>
            </form>
        </div>

);
};

export default InputText;