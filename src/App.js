import React from 'react';
import Blog from "./Containers/Blog/Blog";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import FullPost from "./Components/FullPost/FullPost";
import Subscribe from "./Containers/Subscribe/Subscribe";
import Chat from "./Containers/Chat/Chat";


const App = () => {
  return (
      <BrowserRouter>
          <Switch>
              <Route path="/" exact component={Blog}/>
              <Route path="/fullpost" component={FullPost}/>
              <Route path="/subscribe" component={Subscribe}/>
              <Route path="/chat" component={Chat}/>

          </Switch>
      </BrowserRouter>
  );
};

export default App;
