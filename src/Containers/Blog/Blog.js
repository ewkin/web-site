import React, {useEffect, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import axios, {get} from "axios";

import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import GitHubIcon from '@material-ui/icons/GitHub';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import Header from '../../Components/Header/Header';
import Main from '../../Components/Main/Main';
import Sidebar from '../../Components/Sidebar/Sidebar';
import Footer from '../../Components/Footer/Footer';


const useStyles = makeStyles((theme) => ({
    mainGrid: {
        marginTop: theme.spacing(3),
    },
}));

const sections = [
    { title: 'Home', url: '/' },
];

const sidebar = {
    title: 'About',
    description:
        'Etiam porta sem malesuada magna mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.',
    archives: [
        { title: 'January 2021', url: '/' },

    ],
    social: [
        { name: 'GitHub', icon: GitHubIcon, link:'https://bitbucket.org/ewkin/workspace/projects/NODE'},
        { name: 'Twitter', icon: TwitterIcon, link:'https://twitter.com/navalny' },
        { name: 'Facebook', icon: FacebookIcon, link:'https://www.facebook.com/navalny/' },
    ],
};

const POST_URL = 'posts?_limit=5';
const USER_URL = 'users/'


const Blog = props => {
    const classes = useStyles();
    const [posts, setPosts] = useState([]);


    useEffect(()=>{
        const fetchData = async () =>{
            const postsResponse = await  get(POST_URL);

            const promises = postsResponse.data.map(async post =>{
                const userUrl = USER_URL+post['userId'];
                const userResponse = await axios.get(userUrl);
                return{...post, author: userResponse.data['name']};
            });
            const newPosts = await Promise.all(promises);
            setPosts(newPosts);
        }
        fetchData().catch(console.error);
    },[]);

    const fullPostHandler = id =>{
        const params = new URLSearchParams({'id':id});
        props.history.push({
            pathname: '/fullpost',
            search: '?' + params.toString()
        });
    };
    const subscribeHandler = () =>{
        props.history.push({
            pathname: '/subscribe',
        });
    };
    const chatHandler = () =>{
        props.history.push({
            pathname: '/chat',
        });
    };

    return (
        <React.Fragment>
            <CssBaseline />
            <Container maxWidth="lg">
                <Header title="Blog" sections={sections} chat={chatHandler} subscribe={subscribeHandler} />
                <main>
                    <Grid container spacing={4}>
                    </Grid>
                    <Grid container spacing={5} className={classes.mainGrid}>
                        <Main title="From the firehose" posts={posts} clicked ={fullPostHandler}
                        />
                        <Sidebar
                            title={sidebar.title}
                            description={sidebar.description}
                            archives={sidebar.archives}
                            social={sidebar.social}
                        />
                    </Grid>
                </main>
            </Container>
            <Footer title="Blog Team" description="See you soon." />
        </React.Fragment>
    );
}

export default Blog;
